﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.UI;
using LemonUI;
using LemonUI.Menus;
using RelationAndRiot.Localization;

namespace RelationAndRiot
{
    internal static class Menus
    {
        internal static readonly ObjectPool Pool = new ObjectPool(); // tick it from main directly
        internal static bool Ready { get; private set; } // prevent ticking null
#pragma warning disable S2223 // Non-constant static fields should not be visible
        internal static NativeMenu menu; // sorry but it has to be DONE
#pragma warning restore S2223 // Non-constant static fields should not be visible

#pragma warning disable S1450 // This is annoying
        private static NativeListItem<string> itemGroupOne;
        private static NativeListItem<string> itemGroupTwo;
        private static NativeItem itemDefault;
        private static NativeItem itemRespect;
        private static NativeItem itemLike;
        private static NativeItem itemNeutral;
        private static NativeItem itemNone;
        private static NativeItem itemDislike;
        private static NativeItem itemHate;
        private static NativeCheckboxItem isInRiot;
#pragma warning restore S1450

        internal static void CreateMenus()
        {
            menu = new NativeMenu(Strings.MenuTitle, Strings.MenuSubtitle);
            itemGroupOne = new NativeListItem<string>(Strings.ItemGroupA, Strings.ItemGroupSummary, RelationshipManager.GetRelationshipGroupKeys());
            itemGroupTwo = new NativeListItem<string>(Strings.ItemGroupB, Strings.ItemGroupSummary, RelationshipManager.GetRelationshipGroupKeys());

            itemDefault = new NativeItem(Strings.ItemDefault, Strings.ItemDefaultSummary);
            itemRespect = new NativeItem(Strings.ItemRespect, Strings.ItemRespectSummary);
            itemLike = new NativeItem(Strings.ItemLike, Strings.ItemLikeSummary);
            itemNeutral = new NativeItem(Strings.ItemNeutral, Strings.ItemNeutralSummary);
            itemNone = new NativeItem(Strings.ItemNone, Strings.ItemNoneSummary);
            itemDislike = new NativeItem(Strings.ItemDislike, Strings.ItemDislikeSummary);
            itemHate = new NativeItem(Strings.ItemHate, Strings.ItemHateSummary);
            isInRiot = new NativeCheckboxItem(Strings.ItemRiotMode, Strings.ItemRiotModeSummary, false);

            itemDefault.Activated += (sender, e) => SetRelationshipBetw(Relationship.Companion);
            itemRespect.Activated += (sender, e) => SetRelationshipBetw(Relationship.Respect);
            itemNeutral.Activated += (sender, e) => SetRelationshipBetw(Relationship.Neutral);
            itemNone.Activated += (sender, e) => SetRelationshipBetw(Relationship.Pedestrians);
            itemDislike.Activated += (sender, e) => SetRelationshipBetw(Relationship.Dislike);
            itemHate.Activated += (sender, e) => SetRelationshipBetw(Relationship.Hate);

            isInRiot.CheckboxChanged += (sender, e) =>
            {
                Function.Call((Hash)0x2587A48BC88DFADF, isInRiot.Checked); // LUI Stores it for us
            };

            menu.Add(itemGroupOne);
            menu.Add(itemGroupTwo);
            menu.Add(itemDefault);
            menu.Add(itemRespect);
            menu.Add(itemLike);
            menu.Add(itemNeutral);
            menu.Add(itemNone);
            menu.Add(itemDislike);
            menu.Add(itemHate);
            menu.Add(isInRiot);
            Pool.Add(menu);

            Ready = true;
        }

        internal static void SetRelationshipBetw(Relationship relation)
        {
            var hashOne = RelationshipManager.RelationshipGroups[itemGroupOne.SelectedItem];
            var hashTwo = RelationshipManager.RelationshipGroups[itemGroupTwo.SelectedItem];

            // SHVDN only accepts HASHES
            // Dunno what they will do if it goes over the limit
            var groupOne = new RelationshipGroup(Game.GenerateHash(hashOne));
            var groupTwo = new RelationshipGroup(Game.GenerateHash(hashTwo));

            if (relation != Relationship.Pedestrians) // Bad practice, do not use
            {
                Notification.Show(Strings.SetMessage);
                groupOne.SetRelationshipBetweenGroups(groupTwo, relation); // sets it
            }
            else
            {
                // Prompts the player to set the relationship again
                Notification.Show(Strings.SetTryAgain);
                groupOne.ClearRelationshipBetweenGroups(groupTwo, Relationship.Neutral); // Dunno what it is but let's set it to neutral
            }
        }
    }
}
